# Trimpo demo

This is the demo project to make
1. Parsing YML(format by Yandex)
testing yml - https://www.dropbox.com/s/6yuld2aa1eh506r/yml_test_new.xml?dl=1
real YML клиента http://blandebar.ru/bitrix/catalog_export/eBay_mp30_shmek.xml
2. Group offers by groupId
3. Save all the offers, pictures, params
Using
- Spring boot
- Spring batch
- Liqiubase
- in-memory db
- parsing yml using Stax

### My Notes

```
I didn't see where I should group those offers, and had left
OfferServiceImpl as it is and unused.

```


### Installing

Get a development env running


```
Run the project with mvn clean compile spring-boot:run
If you want to use only jar file - mvn clean compile package

```

## Running the tests

mvn clean compile test

### Break down into end to end tests


```
I have implemented only functional tests.
There is not much logic for unit tests
```

## Deployment

I have used application.properties file to make things easier.
But in a real life projects we can use external property files.

## Built With

* [Spring-Boot](https://projects.spring.io/spring-boot/) - The web framework used
* [Spring-Batch](https://projects.spring.io/spring-batch/) - The batch framework for batch jobs
* [Liquibase](https://www.liquibase.org/) - Keeping database up to date
* [Maven](https://maven.apache.org/) - Dependency Management



## Authors

* **Nurlan Moldabekov** - [Bitbucket profile](https://bitbucket.org/masterhex/)
