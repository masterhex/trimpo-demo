package com.trimpo.repository;

import com.trimpo.entity.ParamEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParamRepository extends JpaRepository<ParamEntity, Long> {

}
