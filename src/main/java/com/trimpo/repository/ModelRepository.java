package com.trimpo.repository;

import com.trimpo.entity.ModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ModelRepository extends JpaRepository<ModelEntity, Long> {

    @Query("SELECT m FROM Model m WHERE m.groupId = :groupId")
    ModelEntity findByGroupId(@Param("groupId") Long groupId);
}
