package com.trimpo;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TrimpoDemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(TrimpoDemoApplication.class, args);
	}
}
