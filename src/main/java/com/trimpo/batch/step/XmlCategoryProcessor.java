package com.trimpo.batch.step;


import com.trimpo.model.Category;
import com.trimpo.model.Offer;
import com.trimpo.repository.CategoryRepository;
import com.trimpo.repository.OfferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

public class XmlCategoryProcessor implements ItemProcessor<Category, Category> {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlCategoryProcessor.class);

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Category process(Category data) throws Exception {
        LOGGER.info("processing category - {}", data.getId());
        LOGGER.debug("processing {}", data);
        categoryRepository.saveAndFlush(data.toEntity());
        return data;
    }

}