package com.trimpo.batch.step;


import com.trimpo.entity.ModelEntity;
import com.trimpo.entity.OfferEntity;
import com.trimpo.entity.ParamEntity;
import com.trimpo.entity.PictureEntity;
import com.trimpo.model.Offer;
import com.trimpo.repository.ModelRepository;
import com.trimpo.repository.OfferRepository;
import com.trimpo.repository.ParamRepository;
import com.trimpo.repository.PictureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class XmlOfferProcessor implements ItemProcessor<Offer, Offer> {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlOfferProcessor.class);

    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private ParamRepository paramRepository;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private ModelRepository modelRepository;

    @Override
    public Offer process(Offer data) throws Exception {
        LOGGER.info("processing Offer - {}", data.getId());
        LOGGER.debug("processing {}", data);
        ModelEntity me = null;
        OfferEntity checkOffer = null;
        if (offerRepository.existsById(data.getId())) {
            checkOffer = offerRepository.getOne(data.getId());
        }

        // Offer is new, groupId is null OR model doesn
        if ((checkOffer==null && data.getGroupId()==null)){
            me = modelRepository.saveAndFlush(new ModelEntity(data.getName(), data.getGroupId()));
            // Offer is new, groupId is not null and we have model with such groupId in DB
        } else if (checkOffer==null && data.getGroupId()!=null
                && modelRepository.findByGroupId(data.getGroupId())!=null){
            me = modelRepository.findByGroupId(data.getGroupId());
        // Offer is not new
        } else if (checkOffer!=null) {
            me = checkOffer.getModel();
        // Model doesn't exist
        } else {
            me = modelRepository.saveAndFlush(new ModelEntity(data.getName(), data.getGroupId()));
        }

        OfferEntity oe = data.toEntity();
        oe.setModel(me);
        final OfferEntity offerEntity =  offerRepository.saveAndFlush(oe);


        Optional.ofNullable(data.getParams()).ifPresent(l -> l.forEach(s -> {
            ParamEntity pe = s.toEntity();
            pe.setOfferEntity(offerEntity);
            paramRepository.save(pe);
        }));

        Optional.ofNullable(data.getPictures()).ifPresent(l -> l.forEach(s -> {
            PictureEntity pe = new PictureEntity(s);
            pe.setOfferEntity(offerEntity);
            pictureRepository.save(pe);
        }));
        return data;
    }

}