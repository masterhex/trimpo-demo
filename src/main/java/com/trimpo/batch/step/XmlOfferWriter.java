package com.trimpo.batch.step;

import com.trimpo.repository.OfferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Deprecated
public class XmlOfferWriter implements ItemWriter<String> {

    private static final Logger logger = LoggerFactory.getLogger(XmlOfferWriter.class);
    @Autowired
    public OfferRepository offerRepository;

    @Override
    public void write(List<? extends String> messages) throws Exception {
        for (String msg : messages) {
            logger.info("Writing the data {}", msg);
        }
    }

}
