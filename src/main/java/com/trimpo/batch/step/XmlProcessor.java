package com.trimpo.batch.step;


import com.trimpo.model.YamlCatalogue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

@Deprecated
public class XmlProcessor implements ItemProcessor<YamlCatalogue, String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlProcessor.class);
    @Override
    public String process(YamlCatalogue data) throws Exception {
        LOGGER.info("process YamlCatalogue - ", data);
        return data.toString();
    }

}