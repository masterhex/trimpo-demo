package com.trimpo.controller;

import com.trimpo.json.ModelJsonResponse;
import com.trimpo.json.OfferJsonResponse;
import com.trimpo.repository.CategoryRepository;
import com.trimpo.repository.ModelRepository;
import com.trimpo.repository.OfferRepository;
import com.trimpo.repository.ParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("")
public class InfoController {
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    OfferRepository offerRepository;
    @Autowired
    ParamRepository paramRepository;
    @Autowired
    ModelRepository modelRepository;

    @RequestMapping("/count")
    public ResponseEntity count() {
        Map<String, Long> map = new HashMap<>();
        map.put("offerCount", offerRepository.count());
        map.put("modelCount", modelRepository.count());
        map.put("paramCount", paramRepository.count());
        map.put("categoryCount", categoryRepository.count());
        return ResponseEntity.ok(map);
    }

    @RequestMapping("/offers")
    public ResponseEntity getAllOffers() {
        return ResponseEntity.ok(offerRepository.findAll().stream().map(s -> new OfferJsonResponse(s)).collect(Collectors.toList()));
    }

    @RequestMapping("/models")
    public ResponseEntity getAllModels() {
        return ResponseEntity.ok(modelRepository.findAll().stream().map(s -> new ModelJsonResponse(s)).collect(Collectors.toList()));
    }

    @RequestMapping("/categories")
    public ResponseEntity getAllCategories() {
        return ResponseEntity.ok(categoryRepository.findAll());
    }
}
