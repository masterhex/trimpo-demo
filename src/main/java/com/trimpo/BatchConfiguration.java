package com.trimpo;


import com.trimpo.batch.enums.UrlType;
import com.trimpo.batch.listener.JobCompletionListener;
import com.trimpo.batch.step.XmlCategoryProcessor;
import com.trimpo.batch.step.XmlOfferProcessor;
import com.trimpo.batch.step.XmlProcessor;
import com.trimpo.model.YamlCatalogue;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.net.MalformedURLException;

/**
 * Created by nurlan on 3/4/18.
 */
@EnableBatchProcessing
@Configuration
public class BatchConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Value("${spring.xml-url}")
    private String xmlUrl;
    @Value("${spring.xml-url-type}")
    private String xmlUrlType;

    public StaxEventItemReader itemReader(String url, String urlType, String rootElement) {

        try {
            StaxEventItemReader itemReader = new StaxEventItemReader();
            if (urlType.equals(UrlType.HTTP.name())) {
                itemReader.setResource(new UrlResource(url));
            } else if (urlType.equals(UrlType.CLASSPATH.name())) {
                Resource file = new ClassPathResource(url);
                itemReader.setResource(file);
            }
            Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
            marshaller.setClassesToBeBound(YamlCatalogue.class);
            itemReader.setUnmarshaller(marshaller);
            itemReader.setFragmentRootElementName(rootElement);
            return itemReader;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Bean
    public Job processJob(Step offerWritingStep, Step categoryWritingStep) {
        return jobBuilderFactory.get("processJob")
                .incrementer(new RunIdIncrementer()).listener(listener()).start(categoryWritingStep)
                .next(offerWritingStep).build();
    }

    @Deprecated
    private Step orderStepOne() {
        return stepBuilderFactory.get("orderStepOne").<String, String>chunk(1)
                .reader(itemReader(xmlUrl,xmlUrlType, "yml_catalog")).processor(new XmlProcessor())
                .build();
    }

    @Bean
    public Step categoryWritingStep(XmlCategoryProcessor xmlCategoryProcessor) {
        return stepBuilderFactory.get("categoryWritingStep").<String, String>chunk(1)
                .reader(itemReader(xmlUrl, xmlUrlType,"category")).processor(xmlCategoryProcessor).build();
    }

    @Bean
    public Step offerWritingStep(XmlOfferProcessor xmlOfferProcessor) {
        return stepBuilderFactory.get("offerWritingStep").<String, String>chunk(1)
                .reader(itemReader(xmlUrl,xmlUrlType, "offer")).processor(xmlOfferProcessor).build();
    }

    @Bean
    public XmlOfferProcessor xmlOfferProcessor() {
        return new XmlOfferProcessor();
    }

    @Bean
    public XmlCategoryProcessor xmlCategoryProcessor() {
        return new XmlCategoryProcessor();
    }

    @Bean
    public JobExecutionListener listener() {
        return new JobCompletionListener();
    }
}
