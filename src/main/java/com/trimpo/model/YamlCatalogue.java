package com.trimpo.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nurlan on 3/4/18.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "yml_catalog")
public class YamlCatalogue {
    @XmlElement(name="shop")
    private Shop shop = null;

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public String toString() {
        return "YamlCatalogue{" +
                "shop=" + shop +
                '}';
    }
}
