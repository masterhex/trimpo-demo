package com.trimpo.model;

import javax.xml.bind.annotation.*;

/**
 * Created by nurlan on 3/5/18.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Currency")
public class Currency {
    @XmlAttribute
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
