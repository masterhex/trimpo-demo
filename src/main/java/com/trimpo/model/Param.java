package com.trimpo.model;

import com.trimpo.entity.ParamEntity;

import javax.xml.bind.annotation.*;

/**
 * Created by nurlan on 3/5/18.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "param")
public class Param {
    @XmlAttribute
    private String name;
    @XmlValue
    private String text;

    public ParamEntity toEntity(){
        ParamEntity paramEntity = new ParamEntity();
        paramEntity.setName(getName());
        paramEntity.setText(getText());
        return paramEntity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Param{" +
                "name='" + name + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
