package com.trimpo.model;

import com.trimpo.entity.ShopEntity;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by nurlan on 3/5/18.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "shop")
public class Shop {
    private String name;
    private String company;
    private String url;

    @XmlElement(name="offers")
    private Offers offers;
    @XmlElement(name="categories")
    private Categories categories;
    @XmlElement(name="currencies")
    private Currencies currencies;

    public ShopEntity toEntity(){
        ShopEntity entity = new ShopEntity();
        entity.setName(getName());
        entity.setCompany(getCompany());
        entity.setUrl(getUrl());
        return entity;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Offers getOffers() {
        return offers;
    }

    public void setOffers(Offers offers) {
        this.offers = offers;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public Currencies getCurrencies() {
        return currencies;
    }

    public void setCurrencies(Currencies currencies) {
        this.currencies = currencies;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "name='" + name + '\'' +
                ", company='" + company + '\'' +
                ", url='" + url + '\'' +
                ", offers=" + offers +
                ", categories=" + categories +
                ", currencies=" + currencies +
                '}';
    }
}
