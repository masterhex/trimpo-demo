package com.trimpo.model;

import com.trimpo.entity.CategoryEntity;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "category")
public class Category {
    @XmlAttribute
    private Long id;
    @XmlValue
    private String name;

    public CategoryEntity toEntity(){
        CategoryEntity entity = new CategoryEntity();
        entity.setId(getId());
        entity.setName(getText());
        return entity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return name;
    }

    public void setText(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
