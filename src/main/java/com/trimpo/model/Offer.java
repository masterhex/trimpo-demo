package com.trimpo.model;

import com.trimpo.entity.OfferEntity;
import com.trimpo.entity.ParamEntity;
import com.trimpo.entity.PictureEntity;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "offer")
public class Offer {
    @XmlAttribute
    private String id;
    @XmlAttribute
    private boolean available;
    @XmlAttribute(name = "group_id")
    private Long groupId;
    private BigDecimal price;
    private String currencyId;
    private Long categoryId;
    private String name;
    private String description;
    private String vendor;
    private String vendorCode;
    private String stock;
    @XmlElement(name="picture")
    private List<String> pictures;
    @XmlElement(name="param")
    private List<Param> params;

    public Offer() {
    }

    public Offer(Long groupId) {
        this.groupId = groupId;
    }

    public OfferEntity toEntity(){
        OfferEntity offerEntity = new OfferEntity();
        offerEntity.setAvailable(isAvailable());
        offerEntity.setId(getId());
        offerEntity.setGroupId(getGroupId());
        offerEntity.setPrice(getPrice());
        offerEntity.setCurrencyId(getCurrencyId());
        offerEntity.setCategoryId(getCategoryId());
        offerEntity.setName(getName());
        offerEntity.setDescription(getDescription());
        offerEntity.setVendor(getVendor());
        offerEntity.setVendorCode(getVendorCode());
        offerEntity.setStock(getStock());
        return offerEntity;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public List<Param> getParams() {
        return params;
    }

    public void setParams(List<Param> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id='" + id + '\'' +
                ", available=" + available +
                ", groupId=" + groupId +
                ", price=" + price +
                ", currencyId='" + currencyId + '\'' +
                ", categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", vendor='" + vendor + '\'' +
                ", vendorCode='" + vendorCode + '\'' +
                ", stock='" + stock + '\'' +
                ", pictures=" + pictures +
                ", params=" + params +
                '}';
    }
}
