package com.trimpo.json;

import com.trimpo.entity.ParamEntity;


public class ParamJsonResponse {
    private Long id;
    private String name;
    private String text;
    private String offerId;

    public ParamJsonResponse(ParamEntity paramEntity) {
        this.id = paramEntity.getId();
        this.name = paramEntity.getName();
        this.text = paramEntity.getText();
        this.offerId = paramEntity.getOfferId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
