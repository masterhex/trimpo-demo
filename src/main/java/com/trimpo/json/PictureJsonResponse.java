package com.trimpo.json;

import com.trimpo.entity.PictureEntity;


public class PictureJsonResponse {
    private Long id;
    private String url;
    private String offerId;


    public PictureJsonResponse(PictureEntity entity) {
        this.id = entity.getId();
        this.url = entity.getUrl();
        this.offerId = entity.getOfferId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
