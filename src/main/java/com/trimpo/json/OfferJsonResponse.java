package com.trimpo.json;

import com.trimpo.entity.*;

import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by nurlan on 3/14/18.
 */
public class OfferJsonResponse {
    private String id;
    private boolean available;
    private Long groupId;
    private Long modelId;
    private BigDecimal price;
    private String currencyId;
    private Long categoryId;
    private String name;
    private String description;
    private String vendor;
    private String vendorCode;
    private String stock;
    private Set<PictureJsonResponse> pictures;
    private Set<ParamJsonResponse> params;
    private ModelJsonResponse model;

    public OfferJsonResponse(OfferEntity entity) {
        this.id = entity.getId();
        this.available = entity.isAvailable();
        this.groupId = entity.getGroupId();
        this.modelId = entity.getGroupId();
        this.price = entity.getPrice();
        this.currencyId = entity.getCurrencyId();
        this.categoryId = entity.getCategoryId();
        this.name = entity.getName();
        this.description = entity.getDescription();
        this.vendor = entity.getVendor();
        this.vendorCode = entity.getVendorCode();
        this.stock = entity.getStock();
        this.pictures = entity.getPictures().stream().map(p -> new PictureJsonResponse(p)).collect(Collectors.toSet());
        this.params = entity.getParams().stream().map(p -> new ParamJsonResponse(p)).collect(Collectors.toSet());
        this.model = new ModelJsonResponse(entity.getModel());
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public Set<PictureJsonResponse> getPictures() {
        return pictures;
    }

    public void setPictures(Set<PictureJsonResponse> pictures) {
        this.pictures = pictures;
    }

    public Set<ParamJsonResponse> getParams() {
        return params;
    }

    public void setParams(Set<ParamJsonResponse> params) {
        this.params = params;
    }

    public ModelJsonResponse getModel() {
        return model;
    }

    public void setModel(ModelJsonResponse model) {
        this.model = model;
    }
}
