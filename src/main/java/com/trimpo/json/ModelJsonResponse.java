package com.trimpo.json;

import com.trimpo.entity.ModelEntity;


public class ModelJsonResponse {
    private Long id;
    private String name;
    private Long groupId;

    public ModelJsonResponse(ModelEntity modelEntity) {
        this.id = modelEntity.getId();
        this.name = modelEntity.getName();
        this.groupId = modelEntity.getGroupId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
}
