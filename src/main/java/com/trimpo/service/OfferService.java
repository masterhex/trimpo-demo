package com.trimpo.service;


import com.trimpo.model.Offer;

import java.util.List;
@Deprecated
public interface OfferService {
    List<List<Offer>> dividingListToSublists(List<Offer> offers);
}
