package com.trimpo.service.impl;


import com.trimpo.model.Offer;
import com.trimpo.service.OfferService;

import java.util.*;
@Deprecated
public class OfferServiceImpl implements OfferService {

//TODO refactor
    @Override
    public List<List<Offer>> dividingListToSublists(List<Offer> offers) {
        Map<Long, List<Offer>> sublists = new HashMap<>();
        List<List<Offer>> loneOffers = new ArrayList<>();
        for (Offer offer : offers){
            //if offer has no groupId it should be added to list as list with lone element
            if (offer.getGroupId() == null){
                List<Offer> ls = new ArrayList<>();
                ls.add(offer);
                loneOffers.add(ls);
                continue;
            }
            List<Offer> list = sublists.get(offer.getGroupId());
            if (list == null) {
                list = new ArrayList<>();
                list.add(offer);
                sublists.put(offer.getGroupId(), list);
            } else {
                list.add(offer);
                sublists.put(offer.getGroupId(), list);
            }
        }
        for (Map.Entry<Long, List<Offer>> e : sublists.entrySet()){
            List<Offer> offersFromMap =  e.getValue();
            loneOffers.add(offersFromMap);
        }
        return loneOffers;
    }
}
