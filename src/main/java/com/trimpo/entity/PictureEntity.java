package com.trimpo.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "picture")
public class PictureEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String url;

    @Column(name = "offerid",insertable = false,updatable = false)
    private String offerId;

    public PictureEntity() {
    }

    public PictureEntity(String url) {
        this.url = url;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "offerid")
    private OfferEntity offerEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public OfferEntity getOfferEntity() {
        return offerEntity;
    }

    public void setOfferEntity(OfferEntity offerEntity) {
        this.offerEntity = offerEntity;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
