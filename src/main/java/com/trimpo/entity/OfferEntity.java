package com.trimpo.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "offer")
public class OfferEntity implements Serializable {
    @Id
    private String id;
    private boolean available;
    @Column(name = "groupid",insertable = false,updatable = false)
    private Long groupId;
    @Column(name = "modelid",insertable = false,updatable = false)
    private Long modelId;
    private BigDecimal price;
    @Column(name = "currencyid")
    private String currencyId;
    @Column(name = "categoryid")
    private Long categoryId;
    private String name;
    private String description;
    private String vendor;
    @Column(name = "vendorcode")
    private String vendorCode;
    private String stock;
    @OneToMany(mappedBy = "offerEntity", fetch = FetchType.LAZY)
    private Set<PictureEntity> pictures;
    @OneToMany(mappedBy = "offerEntity", fetch = FetchType.LAZY)
    private Set<ParamEntity> params;
    @ManyToOne
    @JoinColumn(name = "shopid")
    private ShopEntity shopEntity;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "modelid")
    private ModelEntity model;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }


    //@JoinColumn(name="id", referencedColumnName="offerid")
    public Set<PictureEntity> getPictures() {
        return pictures;
    }

    public void setPictures(Set<PictureEntity> pictures) {
        this.pictures = pictures;
    }

    //@JoinColumn(name="id", referencedColumnName="offerid")
    public Set<ParamEntity> getParams() {
        return params;
    }

    public void setParams(Set<ParamEntity> params) {
        this.params = params;
    }

    public ShopEntity getShopEntity() {
        return shopEntity;
    }

    public void setShopEntity(ShopEntity shopEntity) {
        this.shopEntity = shopEntity;
    }


    public ModelEntity getModel() {
        return model;
    }

    public void setModel(ModelEntity model) {
        this.model = model;
    }
}
