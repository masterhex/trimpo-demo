package com.trimpo.entity;

import javax.persistence.*;

import java.util.List;


@Entity
@Table(name = "shop")
@Deprecated
public class ShopEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String company;
    private String url;
    @OneToMany(mappedBy = "shopEntity", cascade = CascadeType.ALL)
    private List<OfferEntity> offers;
    @OneToMany(mappedBy = "shopEntity", cascade = CascadeType.ALL)
    private List<CategoryEntity> categories;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<OfferEntity> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferEntity> offers) {
        this.offers = offers;
    }

    public List<CategoryEntity> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryEntity> categories) {
        this.categories = categories;
    }
}
