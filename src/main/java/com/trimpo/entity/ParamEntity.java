package com.trimpo.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "param")
public class ParamEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String text;
    @Column(name = "offerid",insertable = false,updatable = false)
    private String offerId;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "offerid")
    private OfferEntity offerEntity;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public OfferEntity getOfferEntity() {
        return offerEntity;
    }

    public void setOfferEntity(OfferEntity offerEntity) {
        this.offerEntity = offerEntity;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }
}
