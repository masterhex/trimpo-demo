package com.trimpo.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity(name = "Model")
@Table(name = "model")
public class ModelEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "groupid")
    private Long groupId;
    @OneToMany(mappedBy = "model")
    private Set<OfferEntity> offers;

    public ModelEntity() {
    }

    public ModelEntity(String name, Long groupId) {
        this.name = name;
        this.groupId = groupId;
    }

    public ModelEntity(Long groupId) {
        this.groupId = groupId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Set<OfferEntity> getOffers() {
        return offers;
    }

    public void setOffers(Set<OfferEntity> offers) {
        this.offers = offers;
    }
}
