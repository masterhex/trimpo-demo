package com.trimpo;

import com.trimpo.model.YamlCatalogue;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by nurlan on 3/4/18.
 */
@Deprecated
public class TestingXmlParser {


    @Test
    @Ignore
    public void parsingTestFile(){



        JAXBContext jc = null;

        YamlCatalogue yamlCatalogue = null;
        Unmarshaller unmarshaller = null;
        XMLInputFactory xmlif = null;
        XMLStreamReader xmler = null;

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            jc = JAXBContext.newInstance(YamlCatalogue.class);
            unmarshaller = jc.createUnmarshaller();
            xmlif = XMLInputFactory.newInstance();
            xmler = xmlif.createXMLStreamReader(classLoader.getResourceAsStream("yml_test_new.xml"));
        } catch (JAXBException | XMLStreamException e) {
            e.printStackTrace();
            Assert.fail();

        }

        //PersonList obj = (PersonList)unmarshaller.unmarshal(xmler);
    }

    private String getFile(String fileName){

        String result = "";

        ClassLoader classLoader = getClass().getClassLoader();
        try {
            result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;

    }
}
