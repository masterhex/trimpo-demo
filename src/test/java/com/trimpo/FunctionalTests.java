package com.trimpo;

import com.trimpo.repository.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FunctionalTests {


	@Autowired
	OfferRepository offerRepository;
	@Autowired
	ParamRepository paramRepository;
	@Autowired
	PictureRepository pictureRepository;
	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	ModelRepository modelRepository;
	@Test
	public void contextLoads() {
	}

	@Test
	public void shouldSaveAllOffers(){
		Assert.assertTrue(offerRepository.count()==5);
	}
	@Test
	public void shouldSaveAllParams(){
		Assert.assertTrue(paramRepository.count()==36);
	}
	@Test
	public void shouldSaveAllPictures(){
		Assert.assertTrue(pictureRepository.count()==13);
	}
	@Test
	public void shouldSaveAllCategories(){
		Assert.assertTrue(categoryRepository.count()==8);
	}

	@Test
	public void shouldSaveAllModels(){
		Assert.assertTrue(modelRepository.count()==4);
	}


}
