package com.trimpo;


import com.trimpo.model.Offer;
import com.trimpo.service.OfferService;
import com.trimpo.service.impl.OfferServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
@Deprecated
public class UnitTests {

    private OfferService offerService;
    private List<Offer> offers;

    @Before
    @Ignore
    public void setUp(){
        offerService = new OfferServiceImpl();
        offers = Arrays.asList(new Offer[]{
                new Offer(1L),new Offer(1L), new Offer(2L),new Offer(2L),
                new Offer(2L),new Offer(),new Offer(),new Offer()
        });
    }

    @Test
    @Ignore
    public void shouldBePositive(){
        List<List<Offer>> result = offerService.dividingListToSublists(offers);
        Assert.assertTrue(result.size() == 5);
    }

    @Test
    @Ignore
    public void shouldNotFailWithEmptyList(){
        offers = Arrays.asList(new Offer[]{});
        List<List<Offer>> result = offerService.dividingListToSublists(offers);
        Assert.assertTrue(result.size() == 0);
    }

    @Test
    @Ignore
    public void shouldBePositiveWithEqualElements(){
        offers = Arrays.asList(new Offer[]{new Offer(1L),new Offer(1L), new Offer(1L),new Offer(1L),
                new Offer(1L),new Offer(1L),new Offer(1L),new Offer(1L)});
        List<List<Offer>> result = offerService.dividingListToSublists(offers);
        Assert.assertTrue(result.size() == 1);
    }


}
